import ActorList from './actor/ActorList';
import FilmDetail from './film/FilmDetail';
import FilmList from './film/FilmList';
import FilmSearch from './film/FilmSearch';
import Rental from './film/Rental';
import GuessGame from './GuessGame';
import Login from './Login';
import Menu from './Menu';
import { BrowserRouter as Router, Routes, Route } 
from 'react-router-dom';
import { useState } from 'react';

function App() {
  let [loggedin, setLoggedin] = useState(false);
  let [role, setRole] = useState('USER');

  if (! loggedin) {
    return <Login doAuth={setLoggedin} doRole={setRole}  />
  }

  return (
    <Router>
      <Menu logout={setLoggedin} role={role} />
      <Routes>
        <Route path='/' element={<FilmList />}></Route>
        <Route path='/actor-list' element={<ActorList />}></Route>
        <Route path='/film-detail/:id' element={<FilmDetail />}></Route>
        <Route path='/film-search' element={<FilmSearch />}></Route>
        
        { role === 'ADMIN' && (
          <>
            <Route path='/rental' element={<Rental />}></Route>
            <Route path='/guess-game' element={<GuessGame />}></Route>
          </>
        )}

        <Route path='/login' element={<Login />}></Route>
      </Routes>
    </Router>
  );
}

export default App;
