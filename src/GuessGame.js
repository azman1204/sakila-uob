import { useRef, useEffect, useState } from "react";

function GuessGame() {
    let num = useRef(0);
    let [id, setId] = useState(0);
    let [noTrial, setNoTrial] = useState(0);
    let [msg, setMsg] = useState('');
    let [win, setWin] = useState(false);

    let doSubmit = () => {
        setNoTrial(noTrial + 1);
        console.log(num.current.value);
        let val = num.current.value;
        let url = `http://localhost:8080/guess-game/guess/${val}/${id}`;
        let param = { method: 'GET' };
        fetch(url, param)
        .then(data => data.json())
        .then(json => {
            console.log(json);
            if (json.status === 'ok') {
                // correct answer
                let msg = json.message + ` after ${noTrial} trials`;
                setMsg(msg);
                setWin(true);
                num.current.value = '';
            } else 
                setMsg(json.message);
        });
    }

    useEffect(() => {
        start();
    },[])

    const start = () => {
        let url = 'http://localhost:8080/guess-game/start';
        let param = { method: 'GET' };
        fetch(url, param)
        .then(data => data.json())
        .then(json => setId(json.id));
    }

    const playAgain = () => {
        start();
        setWin(false);
        setMsg('');
    }

    return (
        <div>
            ID = { id } No of Trial =  { noTrial }
            <h1>Guess Game</h1>
            Guess a number between 0 and 100 <br/>
            <input type="text" ref={num} />
            <button onClick={doSubmit}>Go</button>
            <hr/>
            { msg }
            { win && <button onClick={ playAgain }>Play Again</button>}
        </div>
    );
}

export default GuessGame;