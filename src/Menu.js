import { Link } from "react-router-dom";

function Menu({ logout, role }) {
    const doLogout = () => {
        logout(false);
    }

    return (
        <div className="navbar">
            <h1>Sakila Store</h1>
            <div className="links">
                <Link to="/">Home</Link>
                <Link to="/film-search">Film</Link>
                <Link to="/actor-list">Actor</Link>

                { role === 'ADMIN' && (
                    <>
                        <Link to="/rental">Rental</Link>
                        <Link to="/guess-game">guess-game</Link>
                    </>
                )}

                <a href="##" onClick={ doLogout }>Logout</a>
            </div>
        </div>
    );
}

export default Menu;