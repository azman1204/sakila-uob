import { useState, useEffect } from "react";

function ActorList() {
    let [actors, setActors] = useState([]);

    useEffect(() => {
        let url = 'http://localhost:8080/actor-all';
        let param = { method: 'GET'};
        fetch(url, param)
        .then(data => data.json())
        .then(json => setActors(json))
        .catch(err => console.log(err));
    }, []);

    return (
        <div>
            <h1>Actor List</h1>
            <table className="table table-bordered table-striped table-hover">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>First Name</th>
                        <th>Last Name</th>
                    </tr>
                </thead>
                <tbody>
                    { actors.map((actor, no) => (
                    <tr key={ actor.actorId }>
                        <td>{ no + 1}.</td>
                        <td>{ actor.firstName }</td>
                        <td>{ actor.lastName }</td>
                    </tr>
                    )) }
                </tbody>
            </table>
        </div>
    );
}

export default ActorList;